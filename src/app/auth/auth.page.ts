import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoadingController } from '@ionic/angular';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage implements OnInit {
  isLoading = false;
  isLogin = true;

  constructor(
    private loadingController: LoadingController,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onSubmitForm(form: NgForm) {
    // console.log(form);
    if(!form.valid) {
      return;
    }

    const username = form.value.email;
    const password = form.value.password;

    // console.log(`${username} , ${password}`);

    if(this.isLogin) {

    } else {
      
    }
  }

  onLogin() {
    this.authService.login();
    this.isLoading = true;
    this.loadingController.create({
      message: 'กำลังเข้าสู่ระบบ ...',
      keyboardClose: true
    })
    .then( loadingBox => {
      loadingBox.present();
      setTimeout(() => {
        this.isLoading = false;
        loadingBox.dismiss();
        this.router.navigateByUrl('/restaurant/tabs/find');
      }, 2000);
    });
  }

  onSwitchMode() {
    this.isLogin = !this.isLogin; 
  }

}

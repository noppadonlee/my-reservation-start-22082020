import { Injectable } from '@angular/core';
import { Reservation } from './reservation.model';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { take, tap, delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {
  // private _reservations: Reservation[] = [
  //   {
  //     id: '1',
  //     restaurantId: '1',
  //     userId: '1',
  //     restaurant_title: 'โรตีห้าแยก',
  //     reservation_datetime: '11-08-2020 15:40:03'
  //   }
  // ];

  private _reservations = new BehaviorSubject<Reservation[]>([]);

  get reservations() {
    // return [...this._reservations];
    return this._reservations.asObservable();
  }

  constructor(private authService: AuthService) { }

  newReservation(
    restaurantId: string,
    restaurant_title: string,
    reservation_datetime: Date
  ) {
    const addReservation = new Reservation(
      Math.random().toString(), 
      restaurantId, 
      this.authService.userId, 
      restaurant_title, 
      reservation_datetime
    );

    return this.reservations.pipe(
      take(1), 
      delay(1000),
      tap( reservations => {
      this._reservations.next(reservations.concat(addReservation));
    }))
  }

  deleteReservation(reservationId: string) {
    return this.reservations.pipe(
      take(1),
      delay(1000),
      tap( reservations => {
        this._reservations.next(reservations.filter( rs => rs.id !== reservationId ));
      })
    );
  }
}

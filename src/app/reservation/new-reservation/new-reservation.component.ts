import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Restaurant } from 'src/app/restaurant/restaurant.model';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-new-reservation',
  templateUrl: './new-reservation.component.html',
  styleUrls: ['./new-reservation.component.scss'],
})
export class NewReservationComponent implements OnInit {
  // @Input() chooseRestaurant: Restaurant;
  @ViewChild('form') form: NgForm;

  
  constructor(private modalController: ModalController, private router: Router) { }

  ngOnInit() {}

  doneReserve() {
    // this.modalController.dismiss({message: 'Hello'}, 'confirm');
    if (!this.form.valid || !this.checkValid) {
      return;
    }

    this.modalController.dismiss(
      {
        reservationInfo: {
          restaurantName: this.form.value['restaurant_name'],
          reservationDateTime: new Date(this.form.value['reservation_datetime'])
        }
      },
      'confirm'
    );
  }

  onCancel() {
    this.modalController.dismiss(null, 'cancel');
  }

  checkValid() {
    const chooseDateTime = new Date(this.form.value['reservation_datetime']);
    return chooseDateTime > new Date();
  } 

  getMinYear() {
    return new Date().getFullYear();
  }
}

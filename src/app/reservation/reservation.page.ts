import { Component, OnInit, OnDestroy } from '@angular/core';
import { ReservationService } from './reservation.service';
import { Reservation } from './reservation.model';
import { IonItemSliding } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.page.html',
  styleUrls: ['./reservation.page.scss'],
})
export class ReservationPage implements OnInit, OnDestroy {
  loadedReservations: Reservation[];
  private reservationSubcription: Subscription;

  constructor(private reservationService: ReservationService) { }

  ngOnInit() {
    // this.loadedReservations = this.reservationService.reservations;
    this.reservationSubcription = this.reservationService.reservations.subscribe( reservations => {
      this.loadedReservations = reservations;
    })
  }

  onCancel(reservationId:string, slidingElement: IonItemSliding) {
    slidingElement.close();
    this.reservationService.deleteReservation(reservationId).subscribe();
    // cancel reservation
  }

  ngOnDestroy() {
    this.reservationSubcription.unsubscribe();
  }

}

export class Reservation {

    constructor(
        public id: string,
        public restaurantId: string,
        public userId: string,
        public restaurant_title: string,
        public reservation_datetime: Date
    ) {}
}
import { Injectable } from '@angular/core';
import { Restaurant } from './restaurant.model';

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {
  private _restaurants: Restaurant[] = [
    new Restaurant(
      '1',
      'โรตีห้าแยก',
      '51 ผังเมือง2 สะเตง Yala, Thailand 95000',
      'https://img.wongnai.com/p/1920x0/2015/11/20/a449158a97bf412184358e6103abe06f.jpg',
      '6.5408784',
      '101.2861912'
    ),
    new Restaurant(
      '2',
      'ร้านข้าวหมกไก่กอไผ่',
      '32 ถ.รัฐคำนึง อ.เมือง จ.ยะลา 95000',
      'https://www.halalroute.in.th/wp-content/uploads/2016/01/DSC_0307-1024x687.jpg',
      '6.551778',
      '101.2817597'
    ),
    new Restaurant(
      '3',
      'Anna\'s Breakfast',
      'Sateng, Mueang Yala District, Yala 95000',
      'https://img.wongnai.com/p/1920x0/2019/10/28/033cd63394fe4d469cb653b93b19b5b2.jpg',
      '6.5421534',
      '101.285658'
    )
  ];

  get restaurants() {
    return [...this._restaurants];
  }


  constructor() { }

  getRestaurant(id:string) {
    return { ...this._restaurants.find( p => {
      return p.id === id;
    }) };
  }
}

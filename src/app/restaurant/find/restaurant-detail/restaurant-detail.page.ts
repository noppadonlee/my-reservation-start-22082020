import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, ModalController, LoadingController } from '@ionic/angular';
import { RestaurantService } from '../../restaurant.service';
import { Restaurant } from '../../restaurant.model';
import { ReservationService } from 'src/app/reservation/reservation.service';
import { NewReservationComponent } from 'src/app/reservation/new-reservation/new-reservation.component';

@Component({
  selector: 'app-restaurant-detail',
  templateUrl: './restaurant-detail.page.html',
  styleUrls: ['./restaurant-detail.page.scss'],
})
export class RestaurantDetailPage implements OnInit {
  restaurant: Restaurant;

  constructor(
    private route: ActivatedRoute, 
    private navController: NavController,
    private modalController: ModalController,
    private restaurantService: RestaurantService,
    private router: Router,
    private reservationService: ReservationService,
    private loadingController: LoadingController
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe( paramMap => {
      if(!paramMap.has('restaurantId')) {
        this.navController.navigateBack('/restaurant/tabs/find');
        return;
      }
      this.restaurant = this.restaurantService.getRestaurant(paramMap.get('restaurantId'));
    });
  }

  onReservation() {
    this.modalController
      .create({
        component: NewReservationComponent,
        // componentProps: { chooseRestaurant: this.restaurant }
      })
      .then( modalElement => {
        modalElement.present();
        return modalElement.onDidDismiss();
      })
      .then( result => {
        if (result.role === 'confirm') {
          this.loadingController.create({message: 'กำลังสำรองโต๊ะ'}).then( loadingElement => {
            loadingElement.present();
            const reserveInfo = result.data.reservationInfo;
            this.reservationService.newReservation(
              this.restaurant.id, 
              this.restaurant.name, 
              reserveInfo.reservationDateTime
            ).subscribe( () => {
              loadingElement.dismiss();
            });
          });
          
        }
      });
    // this.reservationService.newReservation(
    //   Math.random().toString(),
    //   this.restaurant.id,
    //   this.authService.userId,
    //   this.restaurant.name,

    // )
    this.router.navigateByUrl('/reservation/new-reservation');
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RestaurantPage } from './restaurant.page';
import { AuthGuard } from '../auth/auth.guard';
import { MapPage } from '../map/map.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/restaurant/tabs/find',
    pathMatch: 'full'
  },
  
  // {
  //   path: 'find',
  //   loadChildren: () => import('./find/find.module').then( m => m.FindPageModule)
  // },
  {
    path: 'tabs',
    component: RestaurantPage,
    children: [
      {
        path: 'find',
        children: [
          {
            path: '',
            loadChildren: () => import('./find/find.module').then( m => m.FindPageModule)
          },
          {
            path: ':restaurantId',
            loadChildren: () => import('./find/restaurant-detail/restaurant-detail.module').then( m => m.RestaurantDetailPageModule)
          }
        ]
        
      },
      {
        
        path: 'map',
        component: MapPage,
        loadChildren: () => import('../map/map.module').then( m => m.MapPageModule),
        canLoad: [AuthGuard]
        
      },
      {
        path: '',
        redirectTo: '/restaurant/tabs/find',
        pathMatch: 'full'
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RestaurantPageRoutingModule {}
